import Data.Map (Map)
import qualified Data.Map as Map
import Data.List (minimumBy, maximumBy, delete, intersect, elemIndex)
import Data.Ord (comparing)
import Text.Printf (printf)
import Data.Maybe (fromJust)

-- GRAPH DATA TYPE -------------------------------------------------------------------------

data Edge = Edge {
    name1 :: String,
    name2 :: String,
    value :: Float
}

type Graph = Map String [(String, Float)]

addEmptyNode :: String -> Graph -> Graph
addEmptyNode node = Map.insertWith (++) node []

addEdge :: Edge -> Graph -> Graph
addEdge edge graph = Map.insertWith (++) (name1 edge) [(name2 edge, value edge)] newGraph
    where newGraph = Map.insertWith (++) (name2 edge) [(name1 edge, value edge)] graph

-- PARSE INPUT -----------------------------------------------------------------------------

buildGraph :: [String] -> Graph
buildGraph input = foldr addEdge Map.empty edges
    where
        edges = map buildEdge input

buildEdge :: String -> Edge
buildEdge lineString = uncurry3 Edge info
    where
        uncurry3 f (a, b, c) = f a b c
        info = getLineInfo lineString

getLineInfo :: String -> (String, String, Float)
getLineInfo lineString = (head namesList, last namesList, 1 / freq)
    where
        stringList = words lineString
        namesList = take 2 stringList
        freq = read $ last stringList

-- ALGORITHM -------------------------------------------------------------------------------

-- |Information on the current state of the algorithm.
data DijkstraInfo = DijkstraInfo {
    distanceAndPrevious :: Map String (Float, String),
    currentNodeSet :: [String]
} deriving (Show)

-- |Receives graph and source node.
-- Returns distances from source and path.
dijkstra :: Graph -> String -> Map String (Float, String)
dijkstra graph source = distanceAndPrevious finalInfo
    where
        initialInfo = initialDijkstraInfo graph
        info = DijkstraInfo (Map.insert source (0, "") $ distanceAndPrevious initialInfo) (currentNodeSet initialInfo)
        nodeNumber = length (currentNodeSet info)
        finalInfo = iterate (dijkstraIteration graph) info !! nodeNumber

-- |Find the minimum distance node and analyze its neighbors.
dijkstraIteration :: Graph -> DijkstraInfo -> DijkstraInfo
dijkstraIteration graph info = foldr (analyzeNeighbor node) newInfo neighborsInSet
    where
        distAndPrev = distanceAndPrevious info
        nodeSet = currentNodeSet info
        node = minimumBy (comparing (fst . (distAndPrev Map.!))) nodeSet
        newNodeSet = delete node nodeSet
        neighborsInSet = filter (\pair -> fst pair `elem` newNodeSet) (graph Map.! node)
        newInfo = DijkstraInfo distAndPrev newNodeSet

-- |Receive the minimum distance node and analyze its neighbors.
-- dijkstraIteration :: Graph -> String -> DijkstraInfo -> DijkstraInfo
-- dijkstraIteration graph node info = foldr (analyzeNeighbor node) info neighborsInSet
--     where
--         distAndPrev = distanceAndPrevious info
--         neighborsInSet = filter (\pair -> fst pair `elem` currentNodeSet info) (graph Map.! node)


-- |Receives node, neighbor, distance between and update information.
analyzeNeighbor :: String -> (String, Float) -> DijkstraInfo -> DijkstraInfo
analyzeNeighbor node (neighbor, distance) info = DijkstraInfo newDistanceAndPrev (currentNodeSet info)
    where
        oldDistAndPrev = distanceAndPrevious info
        nodeDistance = fst (oldDistAndPrev Map.! node)
        newNeighborDistance = nodeDistance + distance
        oldNeighborDistance = fst (oldDistAndPrev Map.! neighbor)
        newDistanceAndPrev = if newNeighborDistance < oldNeighborDistance
            then Map.insert neighbor (newNeighborDistance, node) oldDistAndPrev
            else oldDistAndPrev

-- |Creates initial struct for dijkstra algorithm.
initialDijkstraInfo :: Graph -> DijkstraInfo
initialDijkstraInfo graph = DijkstraInfo distAndPrev nodes
    where
        infinity = read "Infinity" :: Float
        nodes = Map.keys graph
        distAndPrev = foldr (\ node oldInfo -> Map.insert node (infinity, "") oldInfo) Map.empty nodes

--------------------------------------------------------------------------------------------

applyDijkstra :: [String] -> Float
applyDijkstra inputLines = maximum $ map fst (Map.elems dijkstraResult)
    where
        len = fromJust $ elemIndex "" inputLines
        edges = take len inputLines
        source = inputLines !! (len + 1)
        graph = buildGraph edges
        dijkstraResult = dijkstra graph source

main = do
    input <- getContents
    let inputLines = lines input
    let result = applyDijkstra inputLines
    printf "%.2f\n" result
